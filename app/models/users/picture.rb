module Users
  class Picture
    attr_accessor :large, :medium, :thumbnail

    def initialize(params = {})
      @large = params[:large]
      @medium = params[:medium]
      @thumbnail = params[:thumbnail]
    end

    def as_json
      {
        large: @large,
        medium: @medium,
        thumbnail: @thumbnail
      }
    end
  end
end
