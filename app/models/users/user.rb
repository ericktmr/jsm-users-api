module Users
  class User
    TYPE_CLASSIFICATION_BOXES = {
      special: [
        {
          min_latitude: BigDecimal('-2.196998'),
          min_longitude: BigDecimal('-46.361899'),
          max_latitude: BigDecimal('-15.411580'),
          max_longitude: BigDecimal('-34.276938')
        },
        {
          min_latitude: BigDecimal('-19.766959'),
          min_longitude: BigDecimal('-52.997614'),
          max_latitude: BigDecimal('-23.966413'),
          max_longitude: BigDecimal('-44.428305')
        }
      ],
      normal: [
        {
          min_latitude: BigDecimal('-26.155681'),
          min_longitude: BigDecimal('-54.777426'),
          max_latitude: BigDecimal('-34.016466'),
          max_longitude: BigDecimal('-46.603598')
        }
      ]
    }.freeze

    attr_accessor :type, :gender, :name, :location, :email, :birthday, :registered, :telephone_numbers, :mobile_numbers,
                  :picture, :nationality, :first_name, :last_name, :title

    def initialize(params = {})
      @type = params[:type]
      @gender = params[:gender]
      @first_name = params[:first_name]
      @last_name = params[:last_name]
      @title = params[:title]
      @location = params[:location]
      @email = params[:email]
      @birthday = params[:birthday]
      @registered = params[:registered]
      @telephone_numbers = params[:telephone_numbers]
      @mobile_numbers = params[:mobile_numbers]
      @picture = params[:picture]
      @nationality = params[:nationality]
    end

    def define_nationality
      @nationality ||= 'BR'
    end

    # Here we're simplifyng the calculations, it should be enough for short distances like the ones we're handling right now
    def define_type
      raise 'To define a type, user must have defined location' if @location.nil?

      user_point = {
        latitude: BigDecimal(@location.latitude),
        longitude: BigDecimal(@location.longitude)
      }

      TYPE_CLASSIFICATION_BOXES.each do |type, bounding_boxes|
        bounding_boxes.each do |bounding_box|
          if point_inside_box?(user_point, bounding_box)
            @type = type.to_s

            return
          end
        end
      end

      @type = 'laborious'
    end

    def as_json
      {
        type: @type,
        gender: @gender,
        name: {
          title: @title,
          first: @first_name,
          last: @last_name
        },
        location: @location.as_json,
        email: @email,
        birthday: @birthday,
        registered: @registered,
        telephoneNumbers: @telephone_numbers,
        mobileNumbers: @mobile_numbers,
        picture: @picture.as_json,
        nationality: @nationality
      }
    end

    private

    def point_inside_box?(point, bounding_box)
      point[:latitude] >= bounding_box[:max_latitude] && point[:latitude] <= bounding_box[:min_latitude] &&
        point[:longitude] >= bounding_box[:min_longitude] && point[:longitude] <= bounding_box[:max_longitude]
    end
  end
end
