module Users
  class Location
    attr_accessor :region, :street, :city, :state, :postcode, :latitude, :longitude, :timezone_offset,
                  :timezone_description

    def initialize(params = {})
      @region = params[:region]
      @street = params[:street]
      @city = params[:city]
      @state = params[:state]
      @postcode = params[:postcode]
      @latitude = params[:latitude]
      @longitude = params[:longitude]
      @timezone_offset = params[:timezone_offset]
      @timezone_description = params[:timezone_description]
    end

    # We are handling only brazilian regions for now, "unknown" value should help to identify problems
    def define_region
      brazilian_region = get_brazilian_region

      if brazilian_region
        @region ||= brazilian_region
        return
      end

      @region ||= 'unknown'
    end

    def as_json
      {
        region: @region,
        street: @street,
        city: @city,
        state: @state,
        postcode: @postcode,
        coordinates: {
          latitude: @latitude,
          longitude: @longitude
        },
        timezone: {
          offset: @timezone_offset,
          description: @timezone_description
        }
      }
    end

    private

    def get_brazilian_region
      BRAZILIAN_STATE_REGION_HASH.each do |region, states|
        return region if states.any?(I18n.transliterate(@state))
      end

      nil
    end
  end
end
