module Users
  class QueryUserContract < Dry::Validation::Contract
    params do
      optional(:page).maybe(:integer, gt?: 0)
      optional(:page_size).maybe(:integer, gt?: 0)
      optional(:region).maybe(:string, included_in?: BRAZILIAN_STATE_REGION_HASH.keys)
      optional(:type).maybe(:string, included_in?: %w[special normal laborious])
    end

    rule(:page, :page_size) do
      key.failure('page_size must be present if page is present') if values[:page] && values[:page_size].nil?

      if values[:page_size] && values[:page].nil?
        key(:page_size).failure('page must be present if page_size is present')
      end
    end
  end
end
