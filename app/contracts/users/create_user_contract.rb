module Users
  class CreateUserContract < Dry::Validation::Contract
    json do
      required(:gender).value(:string, included_in?: %w[male female])
      required(:latitude).value(:decimal, gteq?: -90, lteq?: 90)
      required(:longitude).value(:decimal, gteq?: -180, lteq?: 180)
      optional(:telephone_number).maybe(:string)
      optional(:mobile_number).maybe(:string)
    end
  end
end
