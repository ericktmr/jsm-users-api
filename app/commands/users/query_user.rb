module Users
  class QueryUser
    def query_paginated(request)
      return { error: request.errors } unless request.valid?

      users = user_repository.get_filtered(region: request.region, type: request.type)

      return build_json(paginated(users, request), users.size, request) if valid_pagination_params?(users.size, request)

      { error: 'Pagination params combination is invalid' }
    end

    private

    def user_repository
      @user_repository ||= UserRepository.new
    end

    def valid_pagination_params?(size, request)
      return true if no_pagination?(request)
      return true if request.page == 1

      if size >= request.page_size
        pages = size / request.page_size
        pages += 1 if (size % request.page_size).positive?

        return request.page <= pages
      end

      false
    end

    def paginated(users, request)
      return users if no_pagination?(request) || (request.page_size >= users.size)

      offset = request.page_size * (request.page - 1)

      users.slice(offset, request.page_size)
    end

    def build_json(paginated_users, total, request)
      {
        'pageNumber' => request.page,
        'pageSize' => request.page_size,
        'totalCount' => total,
        'users' => paginated_users.map(&:as_json)
      }
    end

    def no_pagination?(request)
      request.page.nil? && request.page_size.nil?
    end
  end
end
