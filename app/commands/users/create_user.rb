module Users
  class CreateUser
    GENDER_MAPPER = {
      'male' => 'm',
      'female' => 'f'
    }.freeze
    E164_REGEX = /^\+[1-9]\d{1,14}$/.freeze

    def initialize(user_form)
      @user_form = user_form
    end

    def execute
      if @user_form.valid?
        user = set_user(@user_form)
        location = set_location(@user_form)
        picture = set_picture(@user_form)

        user.location = location
        user.picture = picture
        user.define_type

        user_repository.save(user)

        return
      end

      user_repository.save_unprocessable(@user_form.errors)
      Rails.logger.error "Error processing create_user_form, obj: #{@user_form.inspect}, errors: #{@user_form.errors}"
    end

    private

    def user_repository
      @user_repository ||= UserRepository.new
    end

    def set_user(user_form)
      user = User.new(
        gender: transform_gender(user_form.gender),
        first_name: user_form.first_name,
        last_name: user_form.last_name,
        title: user_form.title,
        email: user_form.email,
        birthday: user_form.birthday,
        registered: user_form.registered,
        telephone_numbers: [transform_phone_number(user_form.telephone_number)],
        mobile_numbers: [transform_phone_number(user_form.mobile_number)],
        nationality: user_form.nationality
      )

      user.define_nationality

      user
    end

    def set_location(user_form)
      location = Location.new(
        street: user_form.street,
        city: user_form.city,
        state: user_form.state,
        postcode: user_form.postcode,
        latitude: user_form.latitude,
        longitude: user_form.longitude,
        timezone_offset: user_form.timezone_offset,
        timezone_description: user_form.timezone_description
      )

      location.define_region

      location
    end

    def set_picture(user_form)
      Picture.new(
        large: user_form.picture_large,
        medium: user_form.picture_medium,
        thumbnail: user_form.picture_thumbnail
      )
    end

    def transform_gender(gender)
      GENDER_MAPPER[gender]
    end

    # Since all the numbers are from Brazil, we are just appending the +55 by now
    def transform_phone_number(number)
      return if number.nil?
      return number if number.match(E164_REGEX)

      digits = number.scan(/\d+/).join

      "+55#{digits}"
    end
  end
end
