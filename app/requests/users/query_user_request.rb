module Users
  class QueryUserRequest
    attr_accessor :region, :type
    attr_writer :page, :page_size

    def initialize(params = {})
      @page = params['page']
      @page_size = params['page_size']
      @region = params['region']
      @type = params['type']
    end

    def valid?
      contract_result.success?
    end

    def errors
      contract_result.errors.to_h
    end

    def page
      return @page.to_i if @page.present? && valid?

      @page
    end

    def page_size
      return @page_size.to_i if @page_size.present? && valid?

      @page_size
    end

    private

    def query_user_contract
      @query_user_contract ||= QueryUserContract.new
    end

    def contract_result
      @contract_result ||= query_user_contract.call(
        page: @page,
        page_size: @page_size,
        region: @region,
        type: @type
      )
    end
  end
end
