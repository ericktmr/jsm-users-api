module Users
  class UserRepository
    DB_KEY = 'database'.freeze
    REGION_TYPE_INDEX_KEY = 'index'.freeze
    DB_UNPROCESSABLE_KEY = 'unprocessable_database'.freeze

    def save(user)
      actual_db = Rails.cache.read(DB_KEY)

      if actual_db.nil?
        Rails.cache.write(DB_KEY, [user])
        Rails.cache.write(
          REGION_TYPE_INDEX_KEY,
          {
            user.location.region => {
              user.type => [0]
            }
          }
        )

        return
      end

      actual_db.push(user)
      new_item_index = actual_db.size - 1
      indexes = Rails.cache.read(REGION_TYPE_INDEX_KEY)
      indexes_appended = append_index(indexes, user, new_item_index)

      Rails.cache.write(DB_KEY, actual_db)
      Rails.cache.write(REGION_TYPE_INDEX_KEY, indexes_appended)
    end

    def save_unprocessable(errors)
      actual_db = Rails.cache.read(DB_UNPROCESSABLE_KEY)

      if actual_db.nil?
        Rails.cache.write(DB_UNPROCESSABLE_KEY, [errors])

        return
      end

      actual_db.push(errors)
      Rails.cache.write(DB_UNPROCESSABLE_KEY, actual_db)
    end

    def get_filtered(filters = {})
      actual_db = Rails.cache.read(DB_KEY)
      indexes = Rails.cache.read(REGION_TYPE_INDEX_KEY)

      if filters[:region]
        region_index = indexes[filters[:region]]

        if filters[:type]
          type_index = region_index[filters[:type]]

          return actual_db.values_at(*type_index)
        end

        return actual_db.values_at(*region_index.values.flatten)
      end

      if filters[:type]
        ids = indexes.values.map do |region_indexes|
          region_indexes[filters[:type]]
        end.flatten.compact

        return actual_db.values_at(*ids)
      end

      actual_db
    end

    private

    def append_index(indexes, user, new_item_index)
      unless indexes.key?(user.location.region)
        return indexes.merge(
          {
            user.location.region => {
              user.type => [new_item_index]
            }
          }
        )
      end

      region_index = indexes[user.location.region]

      unless region_index.key?(user.type)
        new_region_index = region_index.merge(
          {
            user.type => [new_item_index]
          }
        )

        return indexes.merge(user.location.region => new_region_index)
      end

      indexes[user.location.region][user.type] += [new_item_index]

      indexes
    end
  end
end
