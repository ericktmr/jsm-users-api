module Users
  class UsersController < ApplicationController
    def index
      request = QueryUserRequest.new(params)
      response = QueryUser.new.query_paginated(request)

      if response.key?(:error)
        render json: response, status: :unprocessable_entity

        return
      end

      render json: response, status: :ok
    end
  end
end
