module Users
  class CreateUserForm
    attr_accessor :gender, :email, :birthday, :registered, :telephone_number, :mobile_number, :nationality,
                  :first_name, :last_name, :title, :picture_large, :picture_medium, :picture_thumbnail,
                  :street, :city, :state, :postcode, :latitude, :longitude, :timezone_offset, :timezone_description

    def initialize(params = {})
      @gender = params[:gender]
      @first_name = params[:first_name]
      @last_name = params[:last_name]
      @title = params[:title]
      @street = params[:street]
      @city = params[:city]
      @state = params[:state]
      @postcode = params[:postcode]
      @latitude = params[:latitude]
      @longitude = params[:longitude]
      @timezone_offset = params[:timezone_offset]
      @timezone_description = params[:timezone_description]
      @email = params[:email]
      @birthday = params[:birthday]
      @registered = params[:registered]
      @telephone_number = params[:telephone_number]
      @mobile_number = params[:mobile_number]
      @nationality = params[:nationality]
      @picture_large = params[:picture_large]
      @picture_medium = params[:picture_medium]
      @picture_thumbnail = params[:picture_thumbnail]
    end

    def valid?
      contract_result.success?
    end

    def errors
      contract_result.errors.to_h
    end

    private

    def create_user_contract
      @create_user_contract ||= CreateUserContract.new
    end

    def contract_result
      @contract_result ||= create_user_contract.call(
        gender: @gender,
        latitude: @latitude,
        longitude: @longitude,
        telephone_number: @telephone_number,
        mobile_number: @mobile_number
      )
    end
  end
end
