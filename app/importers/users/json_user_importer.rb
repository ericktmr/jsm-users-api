module Users
  class JsonUserImporter
    def initialize(uri)
      @uri = uri
    end

    def import
      response = HTTParty.get(@uri)

      if response.code != 200
        Rails.logger.error "Error fetch users JSON input, code: #{response.code}, message #{response.message}"

        return
      end

      process_response(response)
    end

    private

    def process_response(response)
      if response['results'].nil? || !response['results'].size.positive?
        Rails.logger.error "Error parsing JSON response, JSON: #{response.body}"

        return
      end

      response['results'].each do |user_data|
        user_form = map_data_to_user_form(user_data)

        CreateUser.new(user_form).execute
      end
    end

    def map_data_to_user_form(user_data)
      CreateUserForm.new(
        gender: user_data['gender'],
        title: user_data.dig('name', 'title'),
        first_name: user_data.dig('name', 'first'),
        last_name: user_data.dig('name', 'last'),
        street: user_data.dig('location', 'street'),
        city: user_data.dig('location', 'city'),
        state: user_data.dig('location', 'state'),
        postcode: user_data.dig('location', 'postcode'),
        latitude: user_data.dig('location', 'coordinates', 'latitude'),
        longitude: user_data.dig('location', 'coordinates', 'longitude'),
        timezone_offset: user_data.dig('location', 'timezone', 'offset'),
        timezone_description: user_data.dig('location', 'timezone', 'description'),
        email: user_data['email'],
        birthday: user_data.dig('dob', 'date'),
        registered: user_data.dig('registered', 'date'),
        telephone_number: user_data['phone'],
        mobile_number: user_data['cell'],
        nationality: user_data['nationality'],
        picture_large: user_data.dig('picture', 'large'),
        picture_medium: user_data.dig('picture', 'medium'),
        picture_thumbnail: user_data.dig('picture', 'thumbnail')
      )
    end
  end
end
