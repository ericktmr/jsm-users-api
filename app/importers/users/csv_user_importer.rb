module Users
  class CsvUserImporter
    def initialize(uri)
      @uri = uri
    end

    def import
      response = HTTParty.get(@uri)

      if response.code != 200
        Rails.logger.error "Error fetch users CSV input, code: #{response.code}, message #{response.message}"

        return
      end

      process_response(response)
    end

    private

    def process_response(response)
      if response.body.blank?
        Rails.logger.error "Error parsing CSV response, body: #{response.body}"

        return
      end

      # there is some problem on the CSV encoding, basically we are removing the odd bytes
      encoded_str = response.body.force_encoding('utf-8').encode.sub("\xEF\xBB\xBF", '')

      CSV.parse(encoded_str, headers: true, liberal_parsing: true).each do |line|
        user_form = map_data_to_user_form(line.to_h)

        CreateUser.new(user_form).execute
      end
    end

    def map_data_to_user_form(user_data)
      CreateUserForm.new(
        gender: user_data['gender'],
        title: user_data['name__title'],
        first_name: user_data['name__first'],
        last_name: user_data['name__last'],
        street: user_data['location__street'],
        city: user_data['location__city'],
        state: user_data['location__state'],
        postcode: user_data['location__postcode'],
        latitude: user_data['location__coordinates__latitude'],
        longitude: user_data['location__coordinates__longitude'],
        timezone_offset: user_data['location__timezone__offset'],
        timezone_description: user_data['location__timezone__description'],
        email: user_data['email'],
        birthday: user_data['dob__date'],
        registered: user_data['registered__date'],
        telephone_number: user_data['phone'],
        mobile_number: user_data['cell'],
        nationality: user_data['nationality'],
        picture_large: user_data['picture__large'],
        picture_medium: user_data['picture__medium'],
        picture_thumbnail: user_data['picture__thumbnail']
      )
    end
  end
end
