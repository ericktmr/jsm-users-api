Rails.application.routes.draw do
  scope module: 'users' do
    resources :users, only: [:index]
  end
end
