BRAZILIAN_STATE_REGION_HASH = {
  'sul' => ['parana', 'rio grande do sul', 'santa catarina'],
  'sudeste' => ['sao paulo', 'rio de janeiro', 'espirito santo', 'minas gerais'],
  'centro-oeste' => ['mato grosso', 'mato grosso do sul', 'goias', 'distrito federal'],
  'norte' => ['amazonas', 'roraima', 'amapa', 'para', 'tocantins', 'rondonia', 'acre'],
  'nordeste' => ['maranhao', 'piaui', 'ceara', 'rio grande do norte', 'pernambuco', 'paraiba', 'sergipe', 'alagoas',
                 'bahia']
}.freeze
