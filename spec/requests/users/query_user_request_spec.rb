require 'rails_helper'

RSpec.describe Users::QueryUserRequest do
  let(:instance) { described_class.new(user_attrs) }

  context 'with valid user_attrs' do
    let(:user_attrs) do
      {
        'page' => '1',
        'page_size' => '10',
        'type' => 'laborious',
        'region' => 'sul'
      }
    end

    it 'is valid' do
      expect(instance.valid?).to eq(true)
    end

    it 'has empty errors' do
      expect(instance.errors).to be_empty
    end

    it 'returns an integer page attr' do
      expect(instance.page).to be_an(Integer)
    end

    it 'returns an integer page_size attr' do
      expect(instance.page_size).to be_an(Integer)
    end

    context 'with empty page and page_size' do
      let(:user_attrs) do
        {
          'type' => 'laborious',
          'region' => 'sul'
        }
      end

      it 'returns a null page attr' do
        expect(instance.page).to be_nil
      end

      it 'returns a null page_size attr' do
        expect(instance.page_size).to be_nil
      end
    end
  end

  context 'with invalid attrs' do
    let(:user_attrs) do
      {
        'page' => 'one',
        'page_size' => 'ten',
        'type' => 'laborious',
        'region' => 'sul'
      }
    end

    it 'is not valid' do
      expect(instance.valid?).to eq(false)
    end

    it 'has errors' do
      expect(instance.errors).to_not be_empty
      expect(instance.errors).to eq({ page: ['must be an integer'], page_size: ['must be an integer'] })
    end

    it 'returns the page not casted' do
      expect(instance.page).to eq(user_attrs['page'])
    end

    it 'returns the page_size not casted' do
      expect(instance.page_size).to eq(user_attrs['page_size'])
    end
  end
end
