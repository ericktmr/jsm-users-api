require 'rails_helper'

RSpec.describe Users::CreateUserForm do
  let(:instance) do
    described_class.new(input)
  end

  context 'with valid params' do
    let(:input) do
      {
        gender: 'male',
        latitude: '-87.897',
        longitude: '103.1232'
      }
    end

    it 'is valid' do
      expect(instance.valid?).to eq(true)
    end

    it 'does not have errors' do
      expect(instance.errors).to be_empty
    end
  end

  context 'with invalid params' do
    let(:input) do
      {
        gender: 'male',
        latitude: '-97.897',
        longitude: '103.1232'
      }
    end

    it 'is not valid' do
      expect(instance.valid?).to eq(false)
    end

    it 'has errors' do
      expect(instance.errors).to_not be_empty
    end
  end
end
