require 'rails_helper'

RSpec.describe Users::UserRepository do
  let(:instance) { described_class.new }
  let(:db_key) { Users::UserRepository::DB_KEY }
  let(:index_key) { Users::UserRepository::REGION_TYPE_INDEX_KEY }
  let(:unprocessable_db_key) { Users::UserRepository::DB_UNPROCESSABLE_KEY }

  after do
    Rails.cache.clear
  end

  describe '#save' do
    let(:user) { Users::User.new(location: Users::Location.new(region: 'sudeste'), type: 'special') }
    let(:user_index) { { user.location.region => { user.type => [0] } } }

    context 'with empty cache' do
      it 'creates the index and insert the record' do
        instance.save(user)
        db = Rails.cache.read(db_key)
        index = Rails.cache.read(index_key)

        expect(db.size).to eq(1)
        expect(db.first.type).to eq(user.type)
        expect(db.first.location.region).to eq(user.location.region)
        expect(index).to eq(user_index)
      end
    end

    context 'with populated cache' do
      before do
        instance.save(user)
      end

      it 'appends the record' do
        other_user = Users::User.new(location: Users::Location.new(region: 'sul'), type: 'laborious')
        instance.save(other_user)
        db = Rails.cache.read(db_key)

        expect(db.size).to eq(2)
        expect(db.last.type).to eq(other_user.type)
        expect(db.last.location.region).to eq(other_user.location.region)
      end

      [
        ['sul', 'laborious', { 'sudeste' => { 'special' => [0] }, 'sul' => { 'laborious' => [1] } }],
        ['sudeste', 'laborious', { 'sudeste' => { 'special' => [0], 'laborious' => [1] } }],
        ['sudeste', 'special', { 'sudeste' => { 'special' => [0, 1] } }]
      ].each do |region, type, expected|
        it 'indexes the record correctly' do
          other_user = Users::User.new(location: Users::Location.new(region: region), type: type)
          instance.save(other_user)
          index = Rails.cache.read(index_key)

          expect(index).to eq(expected)
        end
      end
    end
  end

  describe '#save_unprocessable' do
    let(:user_form) { Users::CreateUserForm.new(email: 'some@example.com') }

    context 'with empty cache' do
      it 'appends the unprocessable user_data errors' do
        instance.save_unprocessable(user_form.errors)
        db = Rails.cache.read(unprocessable_db_key)

        expect(db.size).to eq(1)
        expect(db.first).to eq(user_form.errors)
      end
    end

    context 'with a populated cache' do
      before do
        instance.save_unprocessable(user_form.errors)
      end

      it 'appends the unprocessable user_data errors' do
        other_form = Users::CreateUserForm.new(email: 'someone@example.com')
        instance.save_unprocessable(other_form.errors)
        db = Rails.cache.read(unprocessable_db_key)

        expect(db.size).to eq(2)
        expect(db.last).to eq(other_form.errors)
      end
    end
  end

  describe '#get_filtered' do
    let(:users) do
      [
        Users::User.new(
          type: 'normal',
          location: Users::Location.new(region: 'sul')
        ),
        Users::User.new(
          type: 'special',
          location: Users::Location.new(region: 'nordeste')
        ),
        Users::User.new(
          type: 'normal',
          location: Users::Location.new(region: 'norte')
        ),
      ]
    end

    before do
      users.each do |user|
        instance.save(user)
      end
    end

    context 'without filters' do
      it 'returns the entire db' do
        expect(instance.get_filtered.size).to eq(users.size)
        expect(instance.get_filtered.map(&:type)).to match_array(%w[normal normal special])
      end
    end

    context 'with type filter' do
      it 'filters the result by type' do
        expect(instance.get_filtered(type: 'special').size).to eq(1)
        expect(instance.get_filtered(type: 'special').first.type).to eq('special')
      end
    end

    context 'with region filter' do
      it 'filters the result by region' do
        expect(instance.get_filtered(region: 'nordeste').size).to eq(1)
        expect(instance.get_filtered(region: 'nordeste').first.location.region).to eq('nordeste')
      end
    end

    context 'with region and type filters' do
      it 'filters the result by region and type' do
        expect(instance.get_filtered(region: 'norte', type: 'normal').size).to eq(1)
        expect(instance.get_filtered(region: 'norte', type: 'normal').first.type).to eq('normal')
        expect(instance.get_filtered(region: 'norte', type: 'normal').first.location.region).to eq('norte')
      end
    end
  end
end
