require 'rails_helper'

RSpec.describe Users::QueryUserContract do
  let(:contract) { described_class.new }
  let(:result) { contract.call(input) }
  let(:valid_input) do
    {
      page: '1',
      page_size: '10',
      type: 'normal',
      region: 'sudeste'
    }
  end

  context 'with valid params' do
    let(:input) { valid_input }

    it 'succeeds' do
      expect(result.success?).to eq(true)
      expect(result.errors.empty?).to eq(true)
    end
  end

  context 'with empty params' do
    let(:input) { {} }

    it 'succeeds' do
      expect(result.success?).to eq(true)
      expect(result.errors.empty?).to eq(true)
    end
  end

  context 'with page and page_size invalid combination' do
    context 'page present and page_size absent' do
      let(:input) { valid_input.merge(page_size: nil) }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:page]).to eq(['page_size must be present if page is present'])
      end
    end

    context 'page_size present and page absent' do
      let(:input) { valid_input.merge(page: nil) }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:page_size]).to eq(['page must be present if page_size is present'])
      end
    end
  end

  context 'with invalid page param' do
    context 'with page not an integer' do
      let(:input) { valid_input.merge(page: 'ok') }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:page]).to eq(['must be an integer'])
      end
    end

    context 'with page less than 1' do
      let(:input) { valid_input.merge(page: '0') }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:page]).to eq(['must be greater than 0'])
      end
    end
  end

  context 'with invalid page_size param' do
    context 'with page_size not an integer' do
      let(:input) { valid_input.merge(page_size: 'ok') }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:page_size]).to eq(['must be an integer'])
      end
    end

    context 'with page_size less than 1' do
      let(:input) { valid_input.merge(page_size: '0') }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:page_size]).to eq(['must be greater than 0'])
      end
    end
  end

  context 'with invalid region param' do
    let(:input) { valid_input.merge(region: '1') }

    it 'returns errors' do
      expect(result.success?).to eq(false)
      expect(result.errors.empty?).to eq(false)
      expect(result.errors.to_h[:region]).to eq(['must be one of: sul, sudeste, centro-oeste, norte, nordeste'])
    end
  end

  context 'with invalid type param' do
    let(:input) { valid_input.merge(type: '1') }

    it 'returns errors' do
      expect(result.success?).to eq(false)
      expect(result.errors.empty?).to eq(false)
      expect(result.errors.to_h[:type]).to eq(['must be one of: special, normal, laborious'])
    end
  end
end
