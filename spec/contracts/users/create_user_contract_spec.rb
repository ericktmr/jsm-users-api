require 'rails_helper'

RSpec.describe Users::CreateUserContract do
  let(:contract) { described_class.new }
  let(:result) { contract.call(input) }
  let(:valid_input) do
    {
      gender: 'male',
      latitude: '-2.196998',
      longitude: '-46.361899',
      telephone_number: '(01) 5415-5648',
      mobile_number: '(10) 8264-5550'
    }
  end

  context 'with valid params' do
    let(:input) { valid_input }

    it 'succeeds' do
      expect(result.success?).to eq(true)
      expect(result.errors.empty?).to eq(true)
    end
  end

  context 'with invalid gender' do
    let(:input) { valid_input.merge(gender: 'invalid') }

    it 'returns errors' do
      expect(result.success?).to eq(false)
      expect(result.errors.empty?).to eq(false)
      expect(result.errors.to_h[:gender]).to eq(['must be one of: male, female'])
    end
  end

  context 'with invalid latitude' do
    context 'smaller value' do
      let(:input) { valid_input.merge(latitude: '-90.000001') }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:latitude]).to eq(['must be greater than or equal to -90'])
      end
    end

    context 'greater value' do
      let(:input) { valid_input.merge(latitude: '90.000001') }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:latitude]).to eq(['must be less than or equal to 90'])
      end
    end
  end

  context 'with invalid longitude' do
    context 'smaller value' do
      let(:input) { valid_input.merge(longitude: '-180.000001') }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:longitude]).to eq(['must be greater than or equal to -180'])
      end
    end

    context 'greater value' do
      let(:input) { valid_input.merge(longitude: '180.000001') }

      it 'returns errors' do
        expect(result.success?).to eq(false)
        expect(result.errors.empty?).to eq(false)
        expect(result.errors.to_h[:longitude]).to eq(['must be less than or equal to 180'])
      end
    end
  end

  context 'without required fields' do
    let(:required_fields) { %i[gender latitude longitude] }
    let(:input) { valid_input.except(*required_fields) }

    it 'returns errors' do
      expect(result.success?).to eq(false)
      expect(result.errors.empty?).to eq(false)
      expect(result.errors.to_h.keys).to eq(required_fields)
      expect(result.errors.to_h.values.flatten.uniq).to eq(['is missing'])
    end
  end

  context 'without optional fields' do
    let(:optional_fields) { %i[telephone_number mobile_number] }
    let(:input) { valid_input.except(*optional_fields) }

    it 'succeeds' do
      expect(result.success?).to eq(true)
      expect(result.errors.empty?).to eq(true)
    end
  end

  context 'with invalid telephone number' do
    let(:input) { valid_input.merge(telephone_number: 154_155_648) }

    it 'returns errors' do
      expect(result.success?).to eq(false)
      expect(result.errors.empty?).to eq(false)
      expect(result.errors.to_h[:telephone_number]).to eq(['must be a string'])
    end
  end

  context 'with invalid mobile number' do
    let(:input) { valid_input.merge(mobile_number: 154_155_648) }

    it 'returns errors' do
      expect(result.success?).to eq(false)
      expect(result.errors.empty?).to eq(false)
      expect(result.errors.to_h[:mobile_number]).to eq(['must be a string'])
    end
  end
end
