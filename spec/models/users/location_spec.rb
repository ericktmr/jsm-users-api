require 'rails_helper'

RSpec.describe Users::Location do
  describe '#define_region' do
    let(:attrs) { { state: 'rio de janeiro' } }

    it 'sets the correct brazilian region' do
      instance = described_class.new(attrs)

      instance.define_region

      expect(instance.region).to eq('sudeste')
    end

    context 'with special accent characters' do
      let(:attrs) { { state: 'são paulo' } }

      it 'sets the correct brazilian region' do
        instance = described_class.new(attrs)

        instance.define_region

        expect(instance.region).to eq('sudeste')
      end
    end

    context 'with unknown state' do
      let(:attrs) { { state: 'new york' } }

      it 'sets "unknown" value' do
        instance = described_class.new(attrs)

        instance.define_region

        expect(instance.region).to eq('unknown')
      end
    end
  end

  describe '#as_json' do
    let(:expected_keys) do
      %i[region street city state postcode coordinates timezone]
    end

    it 'defines the expected structure' do
      instance = described_class.new

      expect(instance.as_json.keys).to contain_exactly(*expected_keys)
    end
  end
end
