require 'rails_helper'

RSpec.describe Users::User do
  describe '#define_nationality' do
    let(:attrs) { {} }

    it 'sets "BR" as default value' do
      instance = described_class.new(attrs)

      instance.define_nationality

      expect(instance.nationality).to eq('BR')
    end

    context 'with nationality already defined' do
      let(:attrs) { { nationality: 'US' } }

      it 'does not change the nationality' do
        instance = described_class.new(attrs)

        instance.define_nationality

        expect(instance.nationality).to eq('US')
      end
    end
  end

  describe '#define_type' do
    [
      [{ latitude: '-3.196998', longitude: '-44.361899' }, 'special'],
      [{ latitude: '-2.196998', longitude: '-46.361899' }, 'special'],
      [{ latitude: '-15.411580', longitude: '-34.276938' }, 'special'],
      [{ latitude: '-13.411580', longitude: '-36.276938' }, 'special'],
      [{ latitude: '-19.766959', longitude: '-52.997614' }, 'special'],
      [{ latitude: '-20.766959', longitude: '-51.997614' }, 'special'],
      [{ latitude: '-23.966413', longitude: '-44.428305' }, 'special'],
      [{ latitude: '-22.966413', longitude: '-45.428305' }, 'special'],
      [{ latitude: '-26.155681', longitude: '-54.777426' }, 'normal'],
      [{ latitude: '-27.155681', longitude: '-53.777426' }, 'normal'],
      [{ latitude: '-34.016466', longitude: '-46.603598' }, 'normal'],
      [{ latitude: '-33.016466', longitude: '-47.603598' }, 'normal'],
      [{ latitude: '89', longitude: '-180' }, 'laborious']
    ].each do |location_attrs, expected|
      it 'sets the expected type' do
        attrs = { location: Users::Location.new(location_attrs) }
        instance = described_class.new(attrs)

        instance.define_type

        expect(instance.type).to eq(expected)
      end
    end

    context 'without location' do
      let(:attrs) { {} }

      it 'raises and exception' do
        instance = described_class.new(attrs)

        expect { instance.define_type }.to raise_error(RuntimeError, 'To define a type, user must have defined location')
      end
    end
  end

  describe '#as_json' do
    let(:expected_keys) do
      %i[type gender name location email birthday registered telephoneNumbers mobileNumbers picture nationality]
    end

    it 'defines the expected structure' do
      instance = described_class.new

      expect(instance.as_json.keys).to contain_exactly(*expected_keys)
    end
  end
end
