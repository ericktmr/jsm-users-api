require 'rails_helper'

RSpec.describe Users::Picture do
  describe '#as_json' do
    let(:expected_keys) do
      %i[large medium thumbnail]
    end

    it 'defines the expected structure' do
      instance = described_class.new

      expect(instance.as_json.keys).to contain_exactly(*expected_keys)
    end
  end
end
