require 'rails_helper'

RSpec.describe Users::CreateUser do
  let(:instance) { described_class.new(user_form) }
  let(:user_form) { Users::CreateUserForm.new(form_attrs) }

  context 'with an invalid form' do
    let(:form_attrs) { { gender: 'other' } }

    it 'does not save the user' do
      repository = double(save_unprocessable: 'called')
      allow(Users::UserRepository).to receive(:new).and_return repository
      expect(repository).to_not receive(:save)

      instance.execute
    end

    it 'saves the data into the unprocessable db' do
      repository = double
      allow(Users::UserRepository).to receive(:new).and_return repository
      expect(repository).to receive(:save_unprocessable).with(user_form.errors)

      instance.execute
    end
  end

  context 'with valid form' do
    let(:form_attrs) do
      {
        gender: 'female',
        latitude: '-45',
        longitude: '50',
        state: 'acre',
        picture_large: 'picurl',
        telephone_number: '(11) 7654-3221'
      }
    end

    before do
      repository = double(save: 'called')
      allow(Users::UserRepository).to receive(:new).and_return repository
    end

    it 'normalizes the gender' do
      user_double = double.as_null_object
      expect(Users::User).to receive(:new).with(hash_including(gender: 'f')).and_return user_double

      instance.execute
    end

    it 'transforms the phone number into E164 format' do
      user_double = double.as_null_object
      expect(Users::User).to receive(:new).with(hash_including(telephone_numbers: ['+551176543221']))
                                          .and_return user_double

      instance.execute
    end

    it 'defines type and nationality on user' do
      user_double = double(:location= => 'called', :picture= => 'called')
      expect(user_double).to receive(:define_nationality)
      expect(user_double).to receive(:define_type)
      allow(Users::User).to receive(:new).and_return(user_double)

      instance.execute
    end

    it 'defines the location region' do
      location_double = double(latitude: form_attrs[:latitude], longitude: form_attrs[:longitude])
      expect(location_double).to receive(:define_region)
      allow(Users::Location).to receive(:new).and_return(location_double)

      instance.execute
    end

    context 'with an E164 number' do
      let(:form_attrs) do
        {
          gender: 'female',
          latitude: '-45',
          longitude: '50',
          state: 'acre',
          picture_large: 'picurl',
          telephone_number: '+551176543221'
        }
      end

      it 'does not change the number' do
        user_double = double.as_null_object
        expect(Users::User).to receive(:new).with(hash_including(telephone_numbers: ['+551176543221']))
                                            .and_return user_double

        instance.execute
      end
    end
  end
end
