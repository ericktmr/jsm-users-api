require 'rails_helper'

RSpec.describe Users::QueryUser do
  let(:instance) { described_class.new }
  let(:user_request) { Users::QueryUserRequest.new(request_params) }
  let(:response) { instance.query_paginated(user_request) }

  context 'with an invalid request' do
    let(:request_params) { { 'page' => 'one' } }

    it 'returns the errors' do
      expect(response).to eq({ error: user_request.errors })
    end

    it 'does not call the repository' do
      repository = double(get_filtered: 'called')
      allow(Users::UserRepository).to receive(:new).and_return repository
      expect(repository).to_not receive(:get_filtered)

      response
    end
  end

  context 'with a valid request' do
    let(:request_params) do
      {
        'page' => '1',
        'page_size' => '10',
        'type' => 'normal',
        'region' => 'norte'
      }
    end
    let(:users) do
      [
        Users::User.new(gender: 'm', type: 'normal'),
        Users::User.new(gender: 'f', type: 'normal')
      ]
    end

    it 'returns the records as json' do
      repository = double(get_filtered: users)
      allow(Users::UserRepository).to receive(:new).and_return repository

      expect(response).to eq(
        {
          'pageNumber' => user_request.page,
          'pageSize' => user_request.page_size,
          'totalCount' => users.size,
          'users' => users.map(&:as_json)
        }
      )
    end

    context 'without pagination params' do
      let(:request_params) do
        {
          'type' => 'normal',
          'region' => 'norte'
        }
      end

      it 'returns the records as json' do
        repository = double(get_filtered: users)
        allow(Users::UserRepository).to receive(:new).and_return repository

        expect(response).to eq(
          {
            'pageNumber' => user_request.page,
            'pageSize' => user_request.page_size,
            'totalCount' => users.size,
            'users' => users.map(&:as_json)
          }
        )
      end
    end

    context 'with more users than page_size' do
      let(:users) do
        [
          Users::User.new(gender: 'm', type: 'normal'),
          Users::User.new(gender: 'f', type: 'normal'),
          Users::User.new(gender: 'f', type: 'normal')
        ]
      end
      let(:request_params) do
        {
          'page' => '2',
          'page_size' => '2',
          'type' => 'normal',
          'region' => 'norte'
        }
      end

      it 'paginates the response' do
        repository = double(get_filtered: users)
        allow(Users::UserRepository).to receive(:new).and_return repository

        expect(response).to eq(
          {
            'pageNumber' => user_request.page,
            'pageSize' => user_request.page_size,
            'totalCount' => users.size,
            'users' => [users.last.as_json]
          }
        )
      end
    end

    context 'with invalid page and page_size combination' do
      let(:request_params) do
        {
          'page' => '2',
          'page_size' => '10',
          'type' => 'normal',
          'region' => 'norte'
        }
      end

      it 'returns an error as response' do
        repository = double(get_filtered: users)
        allow(Users::UserRepository).to receive(:new).and_return repository

        expect(response).to eq(
          {
            error: 'Pagination params combination is invalid'
          }
        )
      end
    end
  end
end
