require 'rails_helper'

RSpec.describe Users::UsersController, type: :controller do
  describe 'GET index' do
    let(:json_response) { JSON.parse(response.body) }

    context 'with errors response' do
      it 'returns unprocessable_entity status' do
        query_user = double(query_paginated: { error: 'something happened' })
        allow(Users::QueryUser).to receive(:new).and_return(query_user)

        get :index
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'returns the error message' do
        error_response = { 'error' => 'something happened' }
        query_user = double(query_paginated: error_response)
        allow(Users::QueryUser).to receive(:new).and_return(query_user)

        get :index
        expect(json_response).to eq(error_response)
      end
    end

    context 'without errors' do
      it 'returns ok status' do
        query_user = double(query_paginated: { results: ['record'] })
        allow(Users::QueryUser).to receive(:new).and_return(query_user)

        get :index
        expect(response).to have_http_status(:ok)
      end

      it 'returns the query call response' do
        query_response = { 'results' => ['record'] }
        query_user = double(query_paginated: query_response)
        allow(Users::QueryUser).to receive(:new).and_return(query_user)

        get :index
        expect(json_response).to eq(query_response)
      end
    end
  end
end
