require 'rails_helper'

RSpec.describe Users::CsvUserImporter do
  let(:instance) { described_class.new('fake.url') }

  describe '#import' do
    before do
      allow(HTTParty).to receive(:get).and_return(http_response)
    end

    context 'with bad response code' do
      let(:http_response) { double(code: 500) }

      it 'logs the failed response' do
        expect(http_response).to receive(:message)
        expect(http_response).to receive(:code)

        instance.import
      end
    end

    context 'without results' do
      let(:http_response) { double(code: 200) }

      it 'logs the body' do
        expect(http_response).to receive(:body).and_return('')
        expect(http_response).to receive(:body)

        instance.import
      end
    end

    context 'with results from the call' do
      let(:http_response) { double(code: 200) }

      before do
        allow(http_response).to receive(:body).and_return(
          "\"gender\",\"location__coordinates__latitude\",\"location__coordinates__longitude\"\r\n\"male\",\"-2.196998\",\"-46.361899\""
        )
      end

      it 'parses to the form and create user' do
        expect(Users::CreateUser).to receive(:new).and_return(double(execute: 'called'))

        instance.import
      end
    end
  end
end
