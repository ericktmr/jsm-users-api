# Users API

## Proposition

This application reads data from various sources (like JSON or CSV) and exports them as a Restful API endpoint.

## Endpoints

### GET /users

Returns a collection of users, it can be paginated.  
Accepts params as query strings.  

Local URI: http://localhost:3000/users

#### Accepted params

- page, the desired page. Constraints: Integer, greater than 0
- page_size, the desired page_size for each page. Constraints: Integer, greater than 0
- type, filter the results by user type. Constraints: must be a valid type (one of ['normal', 'special', 'laborious'])
- region, filter the results by user region. Constraints: must be a valid brazilian region (one of ['sul', 'sudeste', 'centro-oeste', 'norte', 'nordeste'])

## How to run locally

This project uses [RVM](https://rvm.io/ "RVM's Homepage") to manage Ruby and Rails/Gems.

RVM is not required, you could run the dependencies as global.

With RVM installed run:

```
cd ./ (to the repository folder)

rvm install ruby 2.6.5 (if needed)

gem install bundler -v 2.1.4

bundle install

rails s
```
The application could take a while to import the data, logs will be displayed at the console with the updates of the process.

## Running the test suite

This project uses [Rspec](https://rspec.info/ "Rspec's Homepage") as it's testing tool.

To run the entire test suite, run:

```
cd ./ (to the repository folder)

rspec
```

## Geolocation considerations

The latitude and longitude values from the bouding boxes are inverted, this was done since the new values makes more sense, as they are contained inside brazilian regions.

The map below shows the values utilized.  
https://www.google.com/maps/d/edit?mid=15qouv-YQaiuAoZsGidKAUODFkZp5FPpC&usp=sharing

## Improvements to be considered

- The import process of the data could be parallelized, to run faster. This was not implement here because Rails standard way of doing this is with [Sidekiq](https://github.com/mperham/sidekiq "Sidekiq's Homepage") and, without a database, is very hard to accomplish this because there is no common point between Sidekiq server and application server.
- Better observability structure. Our code is logging just simple messages at the moment, with tooling (like Sentry) and more messages distribution we could have a better observability of our application at runtime.
- Better environment separation. There is no environment behavior separation, we could have a fake data seed in development and test and just process real data in production.
- Containeraze the app. Adding container support should help to deploy and maintain consistency across envrionments, it was not applied since now we have a very simple structure and no external services dependencias.
